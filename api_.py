from _main import ocr_match
from flask import Flask, request, jsonify, render_template, redirect, url_for, render_template_string
from werkzeug import secure_filename
from datetime import datetime
import os
import hashlib
import json
#
def unique_ID(usr, req_time):
    to_hash = str(usr)+str(req_time)
    to_hash = to_hash.encode("utf-8")
    md = hashlib.md5()
    md.update(to_hash)
    return md.hexdigest()
#
#upload_img = True
#upload_rsl = True
#
#
#def img_s3(path_name,filename):
#    import boto3
#    from config import bucketName
#    s3 = boto3.client('s3')
#    s3.upload_file(path_name, bucketName, filename)
#def rsl_rds(hashID,usr,req_time,runtime,final_code,final_rsl):
#    import psycopg2
#    from config import rds_host, rds_database, rds_user, rds_password
#    conn = psycopg2.connect(host=rds_host,database=rds_database, user=rds_user, password=rds_password)
#    cur = conn.cursor()
#    cur.execute('SELECT version()')
#    db_version = cur.fetchone()
#    print(db_version)
#    #cur.execute('INSERT ')
#    cur.close()

UPLOAD_FOLDER = './uploaded/'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 4096 * 4096
app.config['GET_RESULTS'] = './results'
tasks = []

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
#        ocr_match1=ocr_match()
        file = request.files['file']
        usr = request.form['usr']
        req_time = request.form['time']
        hashID = unique_ID(usr,req_time)

        
        
        if file and allowed_file(file.filename.lower()):
#             #tag = 
#             tag = datetime.now().strftime("%Y%m%d%H%M%S")
            filename = hashID +'_'+ secure_filename(file.filename)
            path_name=os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(path_name)
#            if upload_img:
#                img_s3(path_name,filename)
#            mode='string_match'
#            try:
#                runtime, final_code, final_rsl,alts=ocr_match1.pred_main(path_name,mode)
#                headers=['Best match']
#                items=[runtime, final_code, final_rsl]
#            except:
#                headers=['Error! Please try again.']
#                items=[['','','']]
            resp = {'runtime':123,'final_code':123,'final_rsl':'123','alts':'123'}
        else:
            resp = {'error':'wrong file type'}
            runtime=-99999
            final_code=-99999
            final_rsl=-99999
#        if upload_rsl:
#            rsl_rds(hashID,usr,req_time,runtime,final_code,final_rsl)
        return jsonify(resp)
        
        
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8787, debug=True)


    
    
    
    
    
    
